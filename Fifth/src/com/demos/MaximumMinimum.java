package com.demos;

public class MaximumMinimum {

	public static void main(String[] args) {
		int[] arr = {54, 546, 548, 60};
        int[] result=findMaxAndMin(arr);
        System.out.println("Maximum Value : " + result[0]);
        System.out.println("Minimum Value : " + result[1]);
    }

    public static int[] findMaxAndMin(int[] arr) {
        int max = Integer.MIN_VALUE;
        int min = Integer.MAX_VALUE;
        for (int num : arr) {
        	if (num > max) {
        		max = num;
            }
            if (num < min) {
                min = num;
            }
        }
        return new int[]{max, min};
    }
}