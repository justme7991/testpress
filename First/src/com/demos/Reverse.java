package com.demos;

import java.util.Scanner;

public class Reverse {

	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter here : ");
		String n=sc.nextLine();
		
		if (n.length()>=1 && n.length()<=100 && n.matches("[a-z]+")) {
			String rev=reverseString(n);
			System.out.println("Reverse : "+rev);
		}
		else {
			System.out.println("Invalid Input");
		}
	}
	public static String reverseString(String n) {
				
		StringBuilder rev=new StringBuilder();
		for (int i=n.length()-1;i>=0;i--) {
			rev.append(n.charAt(i));
		}
		return rev.toString();
	}
}
