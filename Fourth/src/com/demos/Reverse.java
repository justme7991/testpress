package com.demos;

public class Reverse {
	public static void main(String[] args) {
		int N=988;
		int reversed=reverseNumber(N);
		System.out.println("Reversed Number is : "+reversed);
	}
	public static int reverseNumber(int num) {
		String numStr=Integer.toString(num);
		StringBuilder reversedStr=new StringBuilder();
	for (int i = numStr.length() - 1; i >= 0; i--) {
	    char c = numStr.charAt(i);
	    if (c != '0') {
	        reversedStr.append(c);
	    }
	}
	int reversedNum=Integer.parseInt(reversedStr.toString());
	return reversedNum;
	}
}