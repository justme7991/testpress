package com.demos;

import java.util.Arrays;
import java.util.Comparator;

public class ArrangeLargestNumber {

	public static void main(String[] args) {
		int[] arr= {54,546,548,60};
		String[] strArr=new String[arr.length];
		for (int i=0;i<arr.length;i++) {
			strArr[i]=String.valueOf(arr[i]);
		}
		Arrays.sort(strArr);
		for (int i=strArr.length-1;i>=0;i--) {
			System.out.print(strArr[i]);
		}
	}
	public static class LargestNumberComparator implements Comparator<String> {
		@Override
		public int compare(String a, String b) {
			String order1=a+b;
			String order2=b+a;
			return order2.compareTo(order1);
		}
	}
}
